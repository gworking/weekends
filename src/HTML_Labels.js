import React, { Component } from 'react';
import { Dropdown, Label } from 'semantic-ui-react';

export default class HTML_Labels extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Dropdown text='OP' floating labeled button className='icon'>
                <Dropdown.Menu className="dropdown_menu" direction='left'>
                    <Dropdown.Header content='Buys and Sells' />
                    <Dropdown.Divider />
                    {this.props.events.map(value =>
                        <Dropdown.Item className="dropdown_item">
                            <Label small color='grey' circular>{value.date}</Label>
                            <Label small color='orange'>{value.price > 0 ? 'SELL' : 'BUY'}</Label>
                            <Label small>[{value.stocks} stocks]</Label>
                            <Label small>Net price {value.net_price}</Label>
                            <Label small>[stock only price {value.raw_price}]</Label>
                        </Dropdown.Item>
                    )}
                </Dropdown.Menu>
            </Dropdown>
        )
    }
}
