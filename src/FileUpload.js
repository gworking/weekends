import React, { Component } from 'react';
import HTML_Table from './HTML_Table';
import {
    ParseCsv, build_stocks_object, calculate_each_stocks, calculate_stock_prices, get_stock_prices,
    get_coin_rates, round
} from './Parse';

class TextFileReader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            files: null,
            stocks_present: null,
            stocks_past: null
        }

        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.fileUpload = this.fileUpload.bind(this);
    }

    onChange(e) {
        this.setState({ files: e.target.files });
    }

    fileUpload(file) {
        const reader = new FileReader();
        return new Promise((resolve, reject) => {
            reader.readAsText(file);
            reader.onload = (event) => resolve(reader.result);
        })
    }

    onFormSubmit = async (e) => {
        e.preventDefault(); // Stop form submit
        const fileListAsArray = Array.from(this.state.files);
        let results = [];
        for (let file of fileListAsArray) {
            let fileRead = await this.fileUpload(file);
            let fileParsed = await ParseCsv(fileRead);
            fileParsed.data.shift();
            results = results.concat(fileParsed.data);
        }

        Promise.resolve(build_stocks_object(results))
            .then(stocks => { delete stocks.ISIN; return stocks; })
            .then(stocks => calculate_each_stocks(stocks))
            .then(stocks => calculate_stock_prices(stocks)) // falta calcular el price_non_eur i després contar les fees, ingressos, que de moment estic obviant
            .then(stocks => {
                this.setState({
                    'stocks_present': stocks.stocks_present, // a ref
                    'stocks_past': stocks.stocks_past, // a ref
                    'income': stocks.income, // string, fixed
                    'outcome': stocks.outcome, // string, fixed
                    'net_income': stocks.net_income, // string, fixed
                    'global_fees': stocks.global_fees, // string, fixed
                    'stock_fees': stocks.stock_fees, // string, fixed
                    'stock_dividends': stocks.stock_dividends, // string, fixed
                    'pool': stocks.pool // string, fixed
                });
                return stocks;
            })
            .then(stocks => {
                const one_by_one = (prop, parent, stocks, rates) => {
                    for (let stock in stocks) { // for .. of .. doesn't work on plain objects, they're not iterable
                        let ticker = stocks[stock].ticker;
                        if (ticker) {
                            get_stock_prices(ticker).then((info) => {
                                stocks[stock].stock_price = round(parseFloat(info.get('price')));
                                if (stocks[stock].coin === 'GBP')
                                    stocks[stock].stock_price = round(stocks[stock].stock_price / 100);
                                stocks[stock].stock_change = round(info.get('change'));
                                stocks[stock].market_open = info.get('market_open');

                                if (!stocks[stock].EUR_only) {
                                    let coin = stocks[stock].coin;
                                    stocks[stock].EUR_exchange = rates[coin];
                                } else
                                    stocks[stock].EUR_exchange = 1;

                                stocks[stock].value_invested = round(parseFloat(stocks[stock].total_num_stocks)
                                    * stocks[stock].stock_price);
                                stocks[stock].value_invested_EUR = round(stocks[stock].value_invested
                                    / parseFloat(stocks[stock].EUR_exchange));
                                stocks[stock].value_invested_NET_EUR = round(stocks[stock].value_invested_EUR
                                    + stocks[stock].total_fees
                                    + stocks[stock].total_dividends_EUR);
                                stocks[stock].diff = round(-100 *
                                    (stocks[stock].value_invested
                                        + stocks[stock].total_invested
                                        + stocks[stock].total_recovered)
                                    / stocks[stock].total_invested);
                                stocks[stock].diff_EUR = round(-100 *
                                    (stocks[stock].value_invested_EUR
                                        + stocks[stock].total_invested_EUR
                                        + stocks[stock].total_recovered_EUR)
                                    / stocks[stock].total_invested_EUR);
                                stocks[stock].diff_NET_EUR = round(-100 *
                                    (stocks[stock].value_invested_NET_EUR
                                        + stocks[stock].total_invested_EUR
                                        + stocks[stock].total_recovered_EUR)
                                    / stocks[stock].total_invested_EUR);
                                // gains counting all the stocks, spent is raw, recovered is raw, and the invested is net and includes fees and dividends
                                stocks[stock].NET_total_wins = round(stocks[stock].total_invested_EUR
                                    + stocks[stock].total_recovered_EUR
                                    + stocks[stock].value_invested_NET_EUR);
                                // gains counting only the stocks that we still own, fifo like spent and also is raw, stock price is also raw, I don't count anything else so this is raw
                                stocks[stock].NET_remaining_wins = round(stocks[stock].FIFO_stocks_array_EUR.reduce((a, b) => a + b, 0)
                                    + stocks[stock].FIFO_stocks_array_EUR.length
                                    * stocks[stock].stock_price
                                    / stocks[stock].EUR_exchange);
                                stocks[stock].NET_remaining_wins_diff = round(100 * stocks[stock].NET_remaining_wins
                                    / stocks[stock].FIFO_stocks_array_EUR.reduce((a, b) => a + b, 0));
                                stocks[stock].current_vs_sold = round(100 * (stocks[stock].stock_price - stocks[stock].average_sold_price_per_stock) / stocks[stock].average_sold_price_per_stock);
                                if (isNaN(stocks[stock].current_vs_sold)) // if there'0's no sell price (sold by exchange, change of name, etc), this values will be a nAn
                                    stocks[stock].current_vs_sold = 0;

                                if (!isNaN(stocks[stock].value_invested_NET_EUR)) {
                                    parent.pool.total_value_net_eur += stocks[stock].NET_total_wins;
                                    parent.pool.total_invested_EUR += stocks[stock].total_invested_EUR // invested minus recovered
                                        + stocks[stock].total_recovered_EUR;
                                    if (parent.pool.total_net_invested_EUR === 0)
                                        parent.pool.total_net_invested_EUR += parent.global_fees;
                                    parent.pool.total_net_invested_EUR += stocks[stock].total_net_gain_EUR;
                                    //                                        parent.pool.total_net_invested_EUR += stocks[stock].total_net_gain_EUR
                                    //                                      + stocks[stock].total_dividends_EUR;
                                    parent.pool.total_value_net_eur = round(parent.pool.total_value_net_eur);
                                    parent.pool.total_invested_EUR = round(parent.pool.total_invested_EUR);
                                    parent.pool.total_net_invested_EUR = round(parent.pool.total_net_invested_EUR);
                                    parent.pool.cash = round(parent.net_income + parent.pool.total_net_invested_EUR);
                                    //parent.pool.cash = round(parent.net_income + parent.global_fees + parent.stock_fees + parent.stock_dividends + parent.pool.total_invested_EUR); // the last one is changing every time
                                    parent.pool.performance = round(100 * (parent.net_income + parent.pool.total_value_net_eur) / parent.net_income);
                                } else
                                    console.log(stocks[stock])

                                this.setState({
                                    'stocks_present': parent.stocks_present,
                                    'stocks_past': parent.stocks_past,
                                    'pool': parent.pool
                                });
                            });
                        } else
                            console.log(stocks[stock])
                    }
                }

                get_coin_rates().then((response) => {
                    one_by_one('stocks_present', stocks, stocks.stocks_present, response.rates); // not blocking
                    one_by_one('stocks_past', stocks, stocks.stocks_past, response.rates); // not blocking
                });

                return 1;
            })
            .then(() => console.log(this.state));
    }


    render() {
        if (this.state.stocks_present)
            return (<HTML_Table state={this.state} />);
        else
            return (
                <form onSubmit={this.onFormSubmit}>
                    <h1>File Upload</h1>
                    <input type="file" onChange={this.onChange} multiple />
                    <button type="submit">Upload</button>
                </form>
            )
    }
}

export default TextFileReader;