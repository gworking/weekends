import Papa from 'papaparse';
import axios from 'axios';
import user_stocks from './stocks.json';

export const ParseCsv = (csv_string) => {
    return new Promise((resolve, reject) => {
        Papa.parse(csv_string, {
            delimiter: ',',
            header: false,
            skipEmptyLines: true,
            complete: (results) => resolve(results)
        });
    });
}

export const build_stocks_object = (data) => {
    let stocks = {};
    stocks.stocks_present = {};
    stocks.global_fees = 0;
    stocks.stock_fees = 0;
    stocks.stock_dividends = 0;
    stocks.pool = {};
    stocks.pool.total_invested_EUR = 0;
    stocks.pool.total_value_net_eur = 0;
    stocks.pool.total_net_invested_EUR = 0;
    stocks.pool.performance = 0;
    stocks.pool.cash = 0;
    stocks.income = 0;
    stocks.net_income = 0;
    stocks.outcome = 0;
    data.shift();

    let most_recent_exchange = {}; // I only store the last value for each coin
    let most_recent_stock_EUR_price = []; // only EUR coins, so an array and I assume it will always need a shift() and never a pop() (depending on DeGiro)
    let calculate_recent_exchange;

    for (let rows of data) {
        let new_entry = {};
        let date_stock = rows[0].split('-');
        new_entry.jsdate = new Date(date_stock[2], date_stock[1], date_stock[0]);
        new_entry.date_num = new_entry.jsdate.getTime();
        new_entry.date = new_entry.jsdate.toString();
        new_entry.hour = rows[1];
        new_entry.value_date = rows[2];
        new_entry.name = rows[3];
        new_entry.stock_code = rows[4];
        new_entry.description = rows[5];
        new_entry.coin = rows[7];
        new_entry.amount = rows[8].replace(/,/g, '.');
        new_entry.stock_unique = new_entry.stock_code + new_entry.name.replace(/\W/g, '');

        if (!new_entry.stock_code && new_entry.amount) {
            new_entry.stock_unique = 'OTHERS_' + new_entry.description;
        } else if (!new_entry.stock_code && !new_entry.amount) {
            continue;
        }

        // if there's an exchange from X to EUR belonging to dividends, not to buy/sell
        if (!new_entry.stock_code && new_entry.description.endsWith('Cambio de Divisa')) {
            if (rows[6] && new_entry.amount < 0)
                most_recent_exchange[new_entry.coin] = parseFloat(rows[6].replace(/,/g, '.'));
            else if (!rows[6] && new_entry.amount > 0)
                calculate_recent_exchange = parseFloat(new_entry.amount);
            else if (!rows[6] && new_entry.amount < 0)
                most_recent_exchange[new_entry.coin] = parseFloat(new_entry.amount / calculate_recent_exchange);
            continue;
            // and belonging to buy/sell
        } else if (new_entry.stock_code && new_entry.description.endsWith('Cambio de Divisa')) {
            if (new_entry.coin === 'EUR')
                most_recent_stock_EUR_price.push(new_entry.amount);
            continue;
        }

        if (new_entry.description.startsWith('Conversión Cash Fund')
            //           || new_entry.description.startsWith('ADR/GDR Pass-Through')
            || new_entry.description.startsWith('Cash Fund cambio de precio')
            || new_entry.name.endsWith('NON TRADEABLE, RESR.')) {
            continue;
        }

        if (new_entry.description.startsWith('DEGIRO Compensación')
            || new_entry.description.startsWith('Distribución del Fondo')
            || new_entry.description.startsWith('Comisión de conectividad con el mercado')) {
            stocks.global_fees += parseFloat(new_entry.amount);
            continue;
        } else if (new_entry.description === 'Interés') {
            stocks.global_fees += parseFloat(new_entry.amount);
            continue;
        }

        if (new_entry.description === 'Ingreso') {
            stocks.income += parseFloat(new_entry.amount);
            stocks.net_income += parseFloat(new_entry.amount);
            continue;
        } else if (new_entry.description === 'Retirada') {
            stocks.outcome += parseFloat(new_entry.amount);
            stocks.net_income += parseFloat(new_entry.amount);
            continue;
        }

        // after the "continue's", but before I actually use it
        if (!stocks.stocks_present[new_entry.stock_unique])
            stocks.stocks_present[new_entry.stock_unique] = [];

        // if there's a dividend, we attach here the most recent exchange
        if (new_entry.description.startsWith('Dividendo')
            || new_entry.description.startsWith('Rendimiento de capital')
            || new_entry.description.startsWith('Retención del dividendo')
            || new_entry.description.startsWith('ADR/GDR Pass-Through Fee')) {
            if (new_entry.coin === 'EUR')
                new_entry.exchange_rate = 1;
            else
                new_entry.exchange_rate = most_recent_exchange[new_entry.coin]; // I only store the last value, assume DeGiro won't mess this
            stocks.stocks_present[new_entry.stock_unique].dividends_coin = new_entry.coin;
        }

        if (new_entry.description.startsWith('Compra')
            || new_entry.description.startsWith('Venta')) {
            new_entry.num_of_stocks = new_entry.description.split(' ')[1].replace(/\./g, '');
            new_entry.amount_EUR = most_recent_stock_EUR_price.shift();

            if (!new_entry.amount_EUR && new_entry.coin !== 'EUR') { // this is the case for example when we've acquired stocks as an exchange and not as a buy
                new_entry.amount_EUR = 0;
                new_entry.amount = 0;
                new_entry.special_situation = 'likely an exchange of stocks, so they come for free in this sense'
            }
            if (new_entry.description.startsWith('Venta'))
                new_entry.num_of_stocks = -new_entry.num_of_stocks;
            stocks.stocks_present[new_entry.stock_unique].coin = new_entry.coin;
        }


        stocks.stocks_present[new_entry.stock_unique].push(new_entry);
        stocks.stocks_present[new_entry.stock_unique].name = new_entry.name;
        stocks.stocks_present[new_entry.stock_unique].stock_code = new_entry.stock_code;
    }

    stocks.income = round(stocks.income);
    stocks.net_income = round(stocks.net_income);
    stocks.outcome = round(stocks.outcome);

    return stocks;
}

export const calculate_each_stocks = (stocks) => {
    for (let stock in stocks.stocks_present) { // for .. of .. doesn't work on plain objects, they're not iterable
        let st = stocks.stocks_present[stock];

        let fees = {};
        let dividends = {};
        let dividends_exchange = {};
        let price_EUR = {}; // raw values, not including anything
        let price_non_EUR = {}; // raw values, not including anything
        let num_stocks = {};
        let num_stocks_bought = {};
        let num_stocks_sold = {};
        let date_array = {};

        let EUR_only = 0;
        for (let stock_entry in st) {
            let ste = st[stock_entry];
            if (!ste.name) // it is not an operation but data of the object
                continue;

            date_array[ste.date_num] = ste.jsdate;

            // organize number of stocks by date
            if (ste.num_of_stocks && ste.num_of_stocks !== 0) {
                if (!num_stocks[ste.date_num])
                    num_stocks[ste.date_num] = 0;
                num_stocks[ste.date_num] += parseInt(ste.num_of_stocks);
            }

            // organize number of stocks by bought or sold
            if (ste.num_of_stocks && ste.num_of_stocks > 0) {
                if (!num_stocks_bought[ste.date_num])
                    num_stocks_bought[ste.date_num] = 0;
                num_stocks_bought[ste.date_num] += parseInt(ste.num_of_stocks);
            } else if (ste.num_of_stocks && ste.num_of_stocks < 0) {
                if (!num_stocks_sold[ste.date_num])
                    num_stocks_sold[ste.date_num] = 0;
                num_stocks_sold[ste.date_num] += parseInt(ste.num_of_stocks);
            }

            // organize fees and dividends
            if (ste.description.startsWith('Dividendo')
                || ste.description.startsWith('Rendimiento de capital')
                || ste.description.startsWith('Retención del dividendo')
                || ste.description.startsWith('ADR/GDR Pass-Through Fee')) {
                if (!dividends[ste.date_num])
                    dividends[ste.date_num] = 0;
                dividends[ste.date_num] += parseFloat(ste.amount);
                if (ste.exchange_rate)
                    dividends_exchange[ste.date_num] = ste.exchange_rate;
            } else if (ste.description.startsWith('Coste de la Acción')) {
                // do nothing
            } else if ((ste.description.startsWith('Compra')
                || ste.description.startsWith('Venta'))
                && ste.coin === 'EUR') { // must correlate to a buy or a sold
                if (ste)
                    EUR_only = 1;
                if (!price_EUR[ste.date_num])
                    price_EUR[ste.date_num] = 0;
                price_EUR[ste.date_num] += parseFloat(ste.amount);
            } else if ((ste.description.startsWith('Compra')
                || ste.description.startsWith('Venta'))
                && ste.coin !== 'EUR') { // must correlate to a buy or a sold
                if (!price_non_EUR[ste.date_num])
                    price_non_EUR[ste.date_num] = 0;
                price_non_EUR[ste.date_num] += parseFloat(ste.amount);
                if (!price_EUR[ste.date_num])
                    price_EUR[ste.date_num] = 0;
                price_EUR[ste.date_num] += parseFloat(ste.amount_EUR);
            } else { // others like 'London/Dublin Stamp Duty' and 'Comisión de compra/venta'
                if (!fees[ste.date_num])
                    fees[ste.date_num] = 0;
                fees[ste.date_num] += parseFloat(ste.amount);
            }
        }
        st.EUR_only = EUR_only;
        st.num_stocks = num_stocks;
        st.num_stocks_bought = num_stocks_bought;
        st.num_stocks_sold = num_stocks_sold;
        st.dividends = dividends;
        st.dividends_exchange = dividends_exchange;
        st.fees = fees;
        st.price_EUR = price_EUR;
        st.price_non_EUR = price_non_EUR;
        st.date_array = date_array;
    }
    return stocks;
}

export const calculate_stock_prices = (stocks) => {
    stocks.stocks_past = {};

    // everything is still in stocks_present here     
    for (let stock in stocks.stocks_present) { // for .. of .. doesn't work on plain objects, they're not iterable
        let st = stocks.stocks_present[stock];
        let ref_st = st.price_non_EUR;
        if (st.EUR_only)
            ref_st = st.price_EUR;

        if (user_stocks[st.name])
            st.ticker = user_stocks[st.name];

        st.final_price = {};
        st.final_price_per_stock = {};
        st.events = [];
        st.total_invested = 0;
        st.total_invested_EUR = 0;
        st.total_recovered = 0;
        st.total_recovered_EUR = 0;
        st.total_fees = 0;
        st.total_num_stocks = 0;
        st.total_num_stocks_bought = 0;
        st.total_num_stocks_sold = 0;
        for (let date in ref_st) {
            st.total_num_stocks += parseFloat(st.num_stocks[date]);
            if (isNaN(st.total_num_stocks)) {
                console.log(st)
                console.log(st.num_stocks)
                console.log(date)
                console.log(st.num_stocks[date])
                console.log(parseFloat(st.num_stocks[date]))
            }

            if (ref_st[date] < 0) { // they are buys and not sells
                st.total_invested += parseFloat(ref_st[date]);
                st.total_invested_EUR += parseFloat(st.price_EUR[date]);
            }
            else if (ref_st[date] > 0) { // they are sells
                st.total_recovered += parseFloat(ref_st[date]);
                st.total_recovered_EUR += parseFloat(st.price_EUR[date]);
            }

            if (st.fees[date]) // when there're no fees for an event it is likely that those stocks have been acquired by an exchange
                st.total_fees += parseFloat(st.fees[date]);

            st.final_price[date] = parseInt((ref_st[date] + st.fees[date]) * 100) / 100;
            st.final_price_per_stock[date] = parseInt(((ref_st[date] + st.fees[date]) / Math.abs(st.num_stocks[date])) * 100) / 100;
            if (isNaN(st.final_price[date])) // selling is by exchange, so no money nor fees
                st.final_price[date] = 0;
            if (isNaN(st.final_price_per_stock[date])) // selling is by exchange, so no money nor fees
                st.final_price_per_stock[date] = 0;

            let this_date_obj = st.date_array[date];
            let this_date = this_date_obj.getFullYear() + '.' + (this_date_obj.getMonth() + 1) + '.' + (this_date_obj.getDate() + 1);
            st.events.push({
                key: date,
                date: this_date,
                stocks: st.num_stocks[date],
                raw_price: round(ref_st[date]) + ' ' + st.coin,
                net_price: st.final_price_per_stock[date] + ' ' + st.coin,
                price: st.final_price_per_stock[date]
            });
        }

        st.total_dividends = 0;
        st.total_dividends_EUR = 0;
        st.dividends_EUR = [];
        for (let date in st.dividends) {
            st.total_dividends += parseFloat(st.dividends[date]);
            st.dividends_EUR[date] = round(parseFloat(st.dividends[date] / st.dividends_exchange[date]));
            st.total_dividends_EUR += st.dividends_EUR[date];
        }

        st.total_dividends = round(st.total_dividends);
        st.total_dividends_EUR = round(st.total_dividends_EUR);
        st.total_fees = round(st.total_fees);
        st.total_invested = round(st.total_invested);
        st.total_invested_EUR = round(st.total_invested_EUR);
        st.total_recovered = round(st.total_recovered);
        st.total_recovered_EUR = round(st.total_recovered_EUR);
        st.total_gain = round(st.total_invested + st.total_recovered);
        st.total_net_gain_EUR = round(st.total_invested_EUR + st.total_recovered_EUR + st.total_fees + st.total_dividends_EUR);
        if (st.total_invested)
            st.total_gain_percentage = round(100 * st.total_gain / -st.total_invested);
        else
            st.total_gain_percentage = 0; // acquisition is by exchange, so we've bought them for free in a sense
        if (st.total_invested_EUR)
            st.total_net_gain_EUR_percentage = round(100 * st.total_net_gain_EUR / -st.total_invested_EUR);
        else
            st.total_net_gain_EUR_percentage = 0; // acquisition is by exchange, so we've bought them for free in a sense
        if (isNaN(st.total_net_gain_EUR_percentage))
            console.log(st)

        if (!isNaN(st.total_fees))
            stocks.stock_fees += st.total_fees;
        if (!isNaN(st.total_dividends_EUR))
            stocks.stock_dividends += st.total_dividends_EUR;

        // fill the FIFO arrays to calculate the average buy stock price (of the stocks we still have)
        // this price is the raw price, not the net one
        st.FIFO_stocks_array = [];
        st.FIFO_stocks_array_EUR = [];
        let FIFO_sells = 0;
        for (let date in st.price_EUR) {
            let len = Math.abs(st.num_stocks[date]);
            let raw_price_per_stock = round(st.price_non_EUR[date] / len);
            let raw_price_per_stock_EUR = round(st.price_EUR[date] / len);
            if (raw_price_per_stock_EUR < 0) // this is a buy
                for (let n = 0; n < len; n++) {
                    st.FIFO_stocks_array_EUR.unshift(raw_price_per_stock_EUR);
                    if (!st.EUR_only)
                        st.FIFO_stocks_array.unshift(raw_price_per_stock);
                }
            else // this is a sell
                FIFO_sells += len;
        }
        st.FIFO_stocks_array = st.FIFO_stocks_array.slice(FIFO_sells);
        st.FIFO_stocks_array_EUR = st.FIFO_stocks_array_EUR.slice(FIFO_sells);


        let ref_fifo = st.FIFO_stocks_array;
        if (st.EUR_only)
            ref_fifo = st.FIFO_stocks_array_EUR;

        st.average_bought_price_per_stock = 0;
        if (ref_fifo.length > 0) {
            for (let price of ref_fifo)
                st.average_bought_price_per_stock += parseFloat(price);
            st.average_bought_price_per_stock = round(st.average_bought_price_per_stock / ref_fifo.length);
        } else {
            for (let date in st.num_stocks_bought) {
                st.average_bought_price_per_stock += parseFloat(ref_st[date]);
                st.total_num_stocks_bought += parseFloat(st.num_stocks_bought[date]);
            }
            st.average_bought_price_per_stock = round(st.average_bought_price_per_stock / st.total_num_stocks_bought);
        }

        st.average_sold_price_per_stock = 0;
        for (let date in st.num_stocks_sold) {
            st.average_sold_price_per_stock += parseFloat(ref_st[date]);
            st.total_num_stocks_sold += parseFloat(st.num_stocks_sold[date]);
        }
        st.average_sold_price_per_stock = round(-st.average_sold_price_per_stock / st.total_num_stocks_sold);

        if (st.total_num_stocks === 0) {
            stocks.stocks_past[stock] = st;
            delete stocks.stocks_present[stock];
        }
    }

    stocks.stock_fees = round(stocks.stock_fees);
    stocks.stock_dividends = round(stocks.stock_dividends);

    return stocks;
    //    return getStockPrice(list_of_tickers).then((stock_values) => ({ stocks: stocks, stock_values: stock_values }));
}

export const round = (num) => {
    return parseInt(100 * num) / 100;
}

export const get_coin_rates = async () => {
    let fixer_api = 'b9ce0fa6a2b95cf6ca7b52b59f582a56';
    let fixer_url = '/api/latest?access_key=' + fixer_api + '&base=EUR';
    //    let fixer_url = 'http://data.fixer.io/api/latest?access_key=' + fixer_api + '&base=EUR';
    //    let fixer_url = 'http://data.fixer.io/api/latest?access_key='+fixer_api+'&from=USD&to=EUR&amount=25';
    try {
        let response = await axios.get(fixer_url);
        return (response.data);
    } catch (e) {
    }
}

export const get_stock_prices = async (ticker, second) => {
    //const wait = ms => new Promise((r, j) => setTimeout(r, ms))

    let url = `/quote/${ticker}`;
    let price_now, change, market_open = 0;
    let info = new Map();
    let parser = new DOMParser();
    let response;

    try {
        response = await axios.get(url);
        let parsedHtml = parser.parseFromString(response.data, 'text/html');

        /*
                try {
                    price_now = parseFloat(parsedHtml.querySelectorAll('span[data-reactid="51"]')[0].textContent.split('x')[0]);
                    price_previous = parseFloat(parsedHtml.querySelectorAll('span[data-reactid="41"]')[0].textContent);
                    change = 100 * (price_now - price_previous) / price_previous;
                } catch (e) {
                    //            console.log('1st error => ' + ticker);
                    //            console.log(parsedHtml.querySelectorAll('span[data-reactid="51"]'))
                    //            console.log(e);
                    price_now = '';
                    price_previous = '';
                }
        */

        /*
                alternative = parsedHtml.getElementsByClassName('My(6px) Pos(r) smartphone_Mt(15px)')[0].textContent;
                let data = alternative.split(/([-+\s\(\)])/);
                price_now = parseFloat(data[0]);
                if (isNaN(parseFloat(data[8].slice(0, -1))))
                    change = data[4].slice(0, -1);
                else
                    change = data[7] + data[8].slice(0, -1);
                    */

        try {
            let nodeIterator = document.createNodeIterator(
                parsedHtml,
                NodeFilter.SHOW_ELEMENT,
                (node) => {

                    return (node.textContent.includes('Market open')
                        || node.textContent.includes('At close:'))
                        && node.nodeName.toLowerCase() !== 'script'
                        && node.children.length === 0
                        ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_REJECT;
                }
            );
            let pars = [];
            let currentNode;

            while (currentNode = nodeIterator.nextNode()) {
                if (currentNode.textContent.includes('Market open'))
                    market_open = 1;
                pars.push(currentNode.parentNode.parentNode.firstElementChild);
            }
            price_now = parseFloat(pars[0].textContent);
            change = parseFloat(pars[0].nextElementSibling.textContent.split('(')[1].split('%')[0]);

        } catch (e) {
            console.log('2nd error => ' + ticker);
        }

        if (!change) {
            console.log('no stock value change for today (?) for => ' + ticker)
        }

        info.set('price', price_now * 100 / 100);
        info.set('change', change * 100 / 100);
        info.set('market_open', market_open);
    } catch (e) {
        console.log('error getting the url => ' + ticker);
        console.log(e);
        console.log(url);
        if (!second) {
            console.log('trying a second time => ' + ticker)
            await wait(1000);
            return get_stock_prices(ticker, 1);
        } else
            console.log('not insisting more')
    }

    return info;
}

const wait = ms => new Promise((r, j)=>setTimeout(r, ms))