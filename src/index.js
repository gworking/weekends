import React from 'react';
import ReactDOM from 'react-dom';
import FileUpload from './FileUpload';
import ProductList  from './App';
import registerServiceWorker from './registerServiceWorker';
import 'semantic-ui-css/semantic.min.css';
import './index.css';

ReactDOM.render(
  <div>
  <FileUpload />
  </div>,
  document.getElementById('root')
);

registerServiceWorker();

if (module.hot) {
    module.hot.accept();
}

